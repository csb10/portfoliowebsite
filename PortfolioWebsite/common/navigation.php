<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: loads website navigation on to page
 -->
<header>
    <div class="header_links"> 
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="previousWork.php">Previous Work</a></li>
            <li><a href="references.php">References / Contact</a></li>
            <li><a href="aboutMe.php">About Me</a></li>                     
        </ul>
    </div>
    <div class="contact">
        <?php include('controller/header.php');?>                
    </div>
</header>