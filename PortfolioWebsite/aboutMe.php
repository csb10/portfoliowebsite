<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: about me page going in to a little more depth about myself
 -->
<?php
    session_start();
    $_SESSION['id']=2;
    include_once('common/open.php');
?>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="view/css/main.css">
        <meta charset="UTF-8">
        <title>About Me</title>
    </head>
    <body>
        <?php include('common/navigation.php');?>
        <div class="content">
            <div class="introduction">
                <div class="inside_statement2">
                    <?php include('controller/statements.php');?>
                </div>
            </div>
            <div class="video">
                <iframe width="560" height="315" src="//www.youtube.com/embed/d6gVs2YkIgI" frameborder="0" allowfullscreen></iframe>       
            </div>

        </div>        
        <?php include('common/footer.php'); ?>         
    </body>
</html>