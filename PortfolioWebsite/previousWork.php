<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: previous work page detail some past project work
 -->
<?php
    session_start();
    $_SESSION['id']=4;
    include_once('common/open.php');
?>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="view/css/main.css">
        <meta charset="UTF-8">
        <title>Previous Work</title>
    </head>
    <body>
        <?php include('common/navigation.php');?>
        <div class="content">
            <div class="introduction">
                <div class="inside_statement">
                <h2>Projects</h2>                
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>                        
                        <th>Description</th>
                    </tr>
                    <?php include('controller/projects.php'); ?>                    
                </table>                    
                    <?php include('controller/statements.php');?>
                    <div class="link">
                        <?php include('controller/work.php');?>
                    </div>
                </div>
            </div>            
        </div>
        <?php include('common/footer.php'); ?> 
    </body>
</html>