<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Index page with website introduction 
 -->
<?php
    session_start();
    $_SESSION['id']=1;
    include_once('common/open.php');
    
?>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="view/css/main.css">
        <meta charset="UTF-8">
        <title>Home</title>
    </head>
    <body>
        <?php include('common/navigation.php');?> 
        <div class="content">
        
            <div class="introduction">
                <div class="inside_statement">
                    <?php include('controller/statements.php');?>
                </div>
            </div>

            <div class="left_column">
                <h2>Skills / Knowledge</h2>
                <ul>
                    <?php include('controller/skillType.php'); ?>
                </ul>
            </div>

            <div class="right_column1">
                <h2>Employment History</h2>                
                <table>
                    <tr>
                        <th>Company</th>
                        <th>Position</th>                        
                        <th>Role Description</th>
                        <th>Start Date</th>
                        <th>End Date</th>                        
                    </tr>
                    <?php include('controller/employment.php'); ?>                    
                </table>

                

            </div>

            <div class="right_column2">
                <h2>Education &amp; Qualifications</h2>                
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Subject</th>                        
                        <th>Grade</th>
                        <th>Start Date</th>
                        <th>End Date</th>                        
                    </tr>
                    <?php include('controller/education.php'); ?>                    
                </table>
            </div>

            <div class="right_column3">
                <h2>Hobbies / Interests</h2>                
                <ul>
                    <?php include('controller/hobbies.php'); ?>                      
                </ul>
            </div>
            
        </div> <!-- End Content-->
        <?php include('common/footer.php'); ?> 
    </body>    
</html>