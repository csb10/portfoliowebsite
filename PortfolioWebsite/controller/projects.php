<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets project details form projects table
-->
<?php
    $result = mysqli_query($con, "SELECT projects.name, projects.description, status.statusType "
            . "FROM projects "
            . "INNER JOIN status "
            . "ON projects.status_id=status.id");
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<tr>";
        echo "<td class='sizewidth3'>".$row['name']."</td>";
        echo "<td class='sizewidth3'>".$row['statusType']."</td>";        
        echo "<td class='sizewidth3 des'>".$row['description']."</td>";
        echo "</tr>";
    }
?>

