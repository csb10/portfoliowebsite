<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets education details form school, school_subject, subject_grades and grades tables
-->
<?php
    $result = mysqli_query($con, "SELECT school.name AS schoolName, subject.name AS subjectName, subject.start_date, subject.end_date, grades.type FROM school_subject_grades 
    INNER JOIN school
    ON school_subject_grades.school_id=school.id
    INNER JOIN subject
    ON school_subject_grades.subject_id=subject.id
    INNER JOIN grades
    ON school_subject_grades.grades_id=grades.id
    ORDER BY start_date DESC");
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<tr>";
        echo "<td class='sizewidth2'>".$row['schoolName']."</td>";
        echo "<td class='sizewidth2'>".$row['subjectName']."</td>";        
        echo "<td class='sizewidth2'>".$row['type']."</td>";
        echo "<td class='sizewidth'>".$row['start_date']."</td>";
        echo "<td class='sizewidth'>".$row['end_date']."</td>";            
        echo "</tr>";
    }
?>