<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets a list of skills from skills table
-->
<?php
    $result = mysqli_query($con, "SELECT * FROM skills_skill_type INNER JOIN skills ON skills_skill_type.skills_id = skills.id WHERE skill_type_id = 1");
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<li>".$row['name']."</li>";
    }
?>

