<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets employment details form employer table
-->
<?php
    $result = mysqli_query($con, "SELECT * FROM employer ORDER BY employer.start_date DESC");
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<tr>";
        echo "<td class='sizewidth2'>".$row['name']."</td>";
        echo "<td class='sizewidth2'>".$row['role']."</td>";        
        echo "<td class='sizewidth2'>".$row['description']."</td>";
        echo "<td class='sizewidth'>".$row['start_date']."</td>";
        echo "<td class='sizewidth'>".$row['end_date']."</td>";            
        echo "</tr>";
    }
?>
