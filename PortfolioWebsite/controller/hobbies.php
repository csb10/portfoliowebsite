<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets hobby information from hobbies table 
-->
<?php
    $result = mysqli_query($con, "SELECT * FROM hobbies");
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<li>&bull;&nbsp;".$row['name']."&nbsp;</li>";
    }
?>
