<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets a list of skills types from skill_type table
-->
<?php
$count = 1;

while($count <=7){
    $result = mysqli_query($con, "SELECT * FROM skill_type WHERE skill_type.id =".$count);
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<h3>".$row['name']."</h3>";
    }
    
    $result = mysqli_query($con, "SELECT * FROM skills_skill_type INNER JOIN skills ON skills_skill_type.skills_id = skills.id WHERE skill_type_id =".$count);
    while($row = mysqli_fetch_assoc($result))
    {
        echo "<li>".$row['name']."</li>";
    }
    $count++;
}
?>