<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: reference page wwith information on how to obtain references 
 -->
<?php
    session_start();
    $_SESSION['id']=3;
    include_once('common/open.php');
?>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="view/css/main.css">
        <meta charset="UTF-8">
        <title>References</title>
    </head>
    <body>
        <?php include('common/navigation.php');?>
        <div class="content">
            <div class="introduction">
                <div class="inside_statement">
                    <?php include('controller/statements.php');?>
                </div>
            </div>
            <div class="header">
                <h2>Contact Form</h2>                
            </div>
            <div class="form">
                <form class="email" action="controller/mailer.php" method="post">
                    <p>Name:</p>
                    <input type="text" name="name" required placeholder="Enter your name"/>
                    <p>email:</p>
                    <input type="email" name="email" required placeholder="Enter your email"/>
                    <p>Subject:</p>
                    <input type="text" name="subject" required placeholder="Your subject here"/>
                    <p>Message:</p>
                    <textarea name="message" required placeholder="Your message here"></textarea></p>
                    <input class="send" type="submit" value="Send">
                </form>
            </div> 
        </div>        
        <?php include('common/footer.php'); ?>         
    </body>
</html>        