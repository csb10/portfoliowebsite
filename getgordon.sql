-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2014 at 12:36 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `getgordon`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_form`
--

CREATE TABLE IF NOT EXISTS `contact_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` text NOT NULL,
  `surname` text NOT NULL,
  `email` text NOT NULL,
  `telephone` text NOT NULL,
  `job` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact_form`
--

INSERT INTO `contact_form` (`id`, `firstname`, `surname`, `email`, `telephone`, `job`) VALUES
(1, 'Craig', 'Bentall', 'csb@hotmail', '01423', 'Please can you build a house');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_name` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `rate_name`, `description`) VALUES
(1, 'Standard handyman rates', '&pound;40 + VAT for the first half hour, and then &pound;20 + VAT per half hour after that for all handyman work (this includes plumbing and electrical work) undertaken by me. Transparent charging which ensures customers get value for money.  Standard rates apply Monday  Friday, 8am 6pm.'),
(2, 'Early morning, evening and Saturday rates', 'If it is not convenient to have the jobs done Monday Friday, 8am 6pm, an early morning (7am 8am), evening or Saturday appointment may be arranged the charges are &pound;60 + VAT for the first half hour, and then &pound;30 + VAT per half hour after that.'),
(3, 'Rescheduling or cancelling an appointment', 'I realise that life is not always predictable but if you need to reschedule or cancel please let me know two days before the appointment or a minimum charge will apply.'),
(4, 'Gutter cleaning', 'For cleaning guttering up to three storeys house please get in touch to discuss the property and I will be able to give you a fixed price for this specialist work.  The gutter cleaning service is available for customers in Surrey, South West and West London.');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` text NOT NULL,
  `surname` text NOT NULL,
  `review` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `firstname`, `surname`, `review`) VALUES
(1, 'Craig', 'Bentall', 'Job done as easily as possible; one email to arrange a time which was exactly the time Gordon arrived. Job done with no fuss. Will be calling Gordon again for the next job. Thanks'),
(2, 'Jack', 'Smith', 'A great service! Gordon did lots of different things quickly and efficiently. I definitely recommend him.'),
(3, 'Jack', 'Black', 'Great job by Gordon');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service`, `description`) VALUES
(1, 'Odd jobs', 'Putting up pictures, mirrors, TVs / Hanging curtains, rails and blinds / Assembling flatpack furniture / Draught proofing / Shower screens / Fitting door and window locks / smoke alarms / Carbon monoxide detectors / Child-proofing sockets and cupboards / Fitting stair gates / Replacing sash window cords'),
(2, 'Gutter cleaning', 'Gutter cleaning with specialist equipment / Mending and replacing old gutters / Sorting out blockages in gutters / Repairing guttering / Fixing broken gutters / Sorting out overflows / Annual gutter cleaning'),
(3, 'Carpentry ', 'Building small cupboards and shelves / Door hanging or adjusting / Replacing locks / Door and window frame repairs / Sash window repairs / Installing kitchen units / Laying wooden floors / Rehanging cupboard doors / Fencing repairs');

-- --------------------------------------------------------

--
-- Table structure for table `services_skills`
--

CREATE TABLE IF NOT EXISTS `services_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `services_id` int(11) NOT NULL,
  `skills_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `services_skills`
--

INSERT INTO `services_skills` (`id`, `services_id`, `skills_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(4, 1, 4),
(5, 3, 5),
(6, 3, 6),
(7, 3, 7);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `skill`) VALUES
(1, 'Putting up pictures'),
(2, 'Mending and replacing pipes'),
(3, 'Assembling flatpack furniture'),
(4, 'Mounting TVs and screens and tidying up the cables'),
(5, 'Building shelves and flatpack cupboards'),
(6, 'Hanging new doors or adjusting and rehanging old ones'),
(7, 'Installing kitchen units');

-- --------------------------------------------------------

--
-- Table structure for table `statements`
--

CREATE TABLE IF NOT EXISTS `statements` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `statement` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `statements`
--

INSERT INTO `statements` (`id`, `statement`) VALUES
(1, 'Welcome to my website. I offer a highly professional, reliable and friendly handyman service in West Yorkshire. Highly recommended Yorkshire handyman service for all home maintenance needs - carpentry, plumbing, electrical, flatpacks, decorating - in the Yorkshire areas.'),
(2, 'I can undertake a wide variety of handyman services for you - big and small - everything from hanging blinds to building shelves, and from sorting out a light fitting, to flatpack assembly, putting in a washing machine and decorating. You may have just one odd job, or several which can be sorted out efficiently in one visit by a skilled, professional London handyman - all to an extremely high standard. No job is too small.'),
(3, 'On this page you will find a gallery of previous work that I have carried out.'),
(4, 'A professional, reliable and friendly West Yorkshire handyman service offering good value for customers.'),
(5, 'I have over 25 years experience in the building industry and competent in all building trades.<br>\r\nI can sort out all those handyman jobs which need fixing in one visit from carpentry and electrical, to plumbing and gutter cleaning, flatpacks and patio cleaning, no job is too small.'),
(6, 'Please get in touch to see how I can help sort out any handyman jobs which need doing. My handyman service includes carpentry, electrical, plumbing. flatpacks and decorating all completed to a very high standard by a skilled and reliable expert.<br><br>\r\nI work throughout Yorkshire and the surrounding areas and would be happy to hear from you to discuss your requirements.\r\n<br><br>\r\nPlease call me, Gordon Brown, on 07973 000 123 or 01423 500500\r\n<br><br>\r\nor send me an email via the form on the right, or directly to: hello@gordonbrown.co.uk\r\n<br><br>\r\nI am contactable between 7.30am  8.30pm, 7 days a week.'),
(7, 'Recommended by customers across West Yorkshire, see what they have said about my handyman service.');

-- --------------------------------------------------------

--
-- Table structure for table `work_comments`
--

CREATE TABLE IF NOT EXISTS `work_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `work_comments`
--

INSERT INTO `work_comments` (`id`, `comment`) VALUES
(1, 'A living room I redecorated. Services included: relaying flooring, painting walls and filling wall holes.'),
(2, 'A bathroom I fitted. Services included: bathroom fitting and plumped in.'),
(3, 'A living room redecorated. Services included: decorating walls and fitting blinds.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
